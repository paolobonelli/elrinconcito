# El rinconcito de Male

[ENG] This repository contains a simple draft for the future web of El rinconcito de Male.

[ESP] Este repositorio contiene un simple borrador para la futura web de El rinconcito de Male.

> ## Important notes // Notas importantes:
>
> To work properly with Grunt and Gunt: // Para que Grunt y Gulp puedan funcionar:
>
> > [ENG] Install grunt and gulp globally with: `npm install -g grunt-cli` & `npm install --global gulp-cli`
> >
> > [ESP] Instalar grunt y gulp globalmente con: `npm install -g grunt-cli` Y `npm install --global gulp-cli`

> ## Usage [ENG]
>
> - Clon the repository with git clone `https://paolobonelli@bitbucket.org/paolobonelli/elrinconcito.git`
>
> - Go to the project's directory with `cd ./elrinconcito/`
>
> - Install the dependencies with `npm install --save`
>
> > - Go to branch of evaluation (Week One - Week Three):
> >
> > > - Week One (01): `git checkout semana1`
> > > - Week Two (02): `git checkout semana2`
> > > - Week Three (03): `git checkout semana3`
> >
> > - Start Lite Server with `npm run dev`
>
> > - Go to Week Four evaluation branch:
> >
> > > `git checkout semana4`
> >
> > - Update the distribution directory (./dist)
> >
> > > `gulp build`
> >
> > - Run Lite Server with:
> >
> > > `gulp`

> ## Uso [ESP]
>
> - Clona el repositorio con `git clone https://paolobonelli@bitbucket.org/paolobonelli/elrinconcito.git`
>
> - Ve al directorio del proyecto con `cd ./elrinconcito/`
>
> - Instala las dependencias con `npm install --save`
>
> - Ve a la rama (branch) de la semana deseada con:
>
> > - Ir a las ramas de evaluación (Semana Uno - Semana Tres):
> >
> > > - Semana Uno (01): `git checkout semana1`
> > > - Semana Dos (02): `git checkout semana2`
> > > - Semana Tres (03): `git checkout semana3`
> >
> > - Corre Lite Server con `npm run dev`
>
> > - Ir a la rama de evaluación de la Semana Cuatro:
> >
> > > `git checkout semana4`
> >
> > - Actualizar la carpeta de producción (./dist)
> >
> > > `gulp build`
> >
> > - Corre Lite Server con:
> >
> > > `gulp`
>
> - Enjoy!
