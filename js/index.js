$(function () {
  $('[data-toggle="popover"]').popover();
});
$(".carousel").carousel({
  interval: 3000,
});
$("[data-target='#shoppingCart']").on("click", function (e) {
  console.log(
    "Se precionó un botón para agregar una salsa al carrito\n "
  );
  var classes = e.target.classList;
  classes.add("cta-disabled");
  console.log("Se desabilitó el botón usado.");
});
$("#shoppingCart")
  .on("show.bs.modal", function (e) {
    console.log(
      "Se está cargando el modal para agregar un producto\n"
    );
  })
  .on("shown.bs.modal", function (e) {
    console.log("Se cargó el modal para agregar un producto\n");
  })
  .on("hide.bs.modal", function (e) {
    console.log("Se decidió salir del modal");
  })
  .on("hidden.bs.modal", function (e) {
    console.log("Se escondió el modal");
    var disabledBtns = document.querySelectorAll(
      "[data-target='#shoppingCart'].cta-disabled"
    );
    var disabledBtnsArray = [disabledBtns];
    disabledBtns.forEach(function (disBtn) {
      disBtn.classList.remove("cta-disabled");
      console.log("Se habilitó el botón que se usó anteriormente");
    });
  });
